//LOAD REQUIRED MODULES
var express = require('express');
var fs = require('fs');
var _ = require('lodash');
var querystring = require('querystring');
var bodyParser = require('body-parser');
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var jsonServer = require('json-server');
var faker = require('Faker');

//DEFINE CONSTANT
var JWT_SECRET = 'data-tablar-secret-key';
var REQUEST_EXPIRES_IN = 30 * 60;
var REFRESH_EXPIRES_IN = 60 * 60;

//INIT THE SERVER
var server = express();
server.set('port', (process.env.PORT || 5001));


//USE MIDDLEWARES
//CORS middleware. Allow request from your application
server.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization"); //Accept Authorization for jwt token Bearer
	next();
});

//USE MIDDLEWARES
//bodyParser for parsing request's payload.
server.use(bodyParser.json()); // for parsing application/json
server.use(bodyParser.urlencoded({
	extended: true
})); // for parsing application/x-www-form-urlencoded

//USE MIDDLEWARES
//expressJWT middleware.
server.use(expressJWT({
	secret: JWT_SECRET,
	getToken: function fromHeader(req) { //Only accept authorization from token.
		if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
			return req.headers.authorization.split(' ')[1];
		}
		return null;
	}
}).unless({
	path: [ //ignore these paths, not checking auth token
		'/api/authors',
		'/login',
		'/refreshToken', {
			url: '/api/posts', // /posts and /posts/
			methods: ['GET']
		}, {
			url: '/api/posts/', // /posts and /posts/
			methods: ['GET']
		} //ignore this path because GUESS can access and read-only posts.
	]
}));
//USE MIDDLEWARES
//Catch invalid request error from express-jwt.
server.use(function(err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.status(401).send('invalid token...');
	}
});

//prepare database
var authors = ["pablo.johnson", "bill.carroll", "Jamir85", "Jacky.Christiansen ", "Velda.Hahn", "admin.istrator"];
var db = {
	authors: authors,
	posts: _.times(500, function(i) {
		return {
			id: i,
			title: faker.Lorem.sentence(),
			content: faker.Lorem.paragraphs(),
			author: authors[i % 6]
		}
	})
}

fs.writeFileSync('db.json', JSON.stringify(db, null, 4), 'utf-8');

//ROUTING - Login - POST
server.post('/login', function(req, res) {
	if (req.body.hasOwnProperty('email') && req.body.hasOwnProperty('password')) {
		if (req.body.password !== 'datatablar') {
			return res.status(403).end();
		}
		var payload = {};
		switch (req.body.email) {
			case 'admin@datatablar.com':
				payload.role = 'admin';
				payload.name = 'Administrator';
				payload.username = 'admin.istrator';
				res.status(200).json({
					request_token: jwt.sign(payload, JWT_SECRET, {
						subject: 'request_token',
						jwtid: JWT_SECRET + Date.now(),
						expiresIn: REQUEST_EXPIRES_IN
					}),
					refresh_token: jwt.sign(payload, JWT_SECRET, {
						subject: 'refresh_token',
						jwtid: JWT_SECRET + Date.now(),
						expiresIn: REFRESH_EXPIRES_IN
					})
				});
				break;

			case 'bill.carroll@datatablar.com':
				payload.role = 'user';
				payload.name = 'Bill Carroll';
				payload.username = 'bill.carroll';
				res.status(200).json({
					request_token: jwt.sign(payload, JWT_SECRET, {
						subject: 'request_token',
						jwtid: JWT_SECRET + Date.now(),
						expiresIn: REQUEST_EXPIRES_IN
					}),
					refresh_token: jwt.sign(payload, JWT_SECRET, {
						subject: 'refresh_token',
						jwtid: JWT_SECRET + Date.now(),
						expiresIn: REFRESH_EXPIRES_IN
					})
				});
				break;

			case 'pablo.johnson@datatablar.com':
				payload.role = 'user';
				payload.name = 'Pablo Johnson';
				payload.username = 'pablo.johnson';
				res.status(200).json({
					request_token: jwt.sign(payload, JWT_SECRET, {
						subject: 'request_token',
						jwtid: JWT_SECRET + Date.now(),
						expiresIn: REQUEST_EXPIRES_IN
					}),
					refresh_token: jwt.sign(payload, JWT_SECRET, {
						subject: 'refresh_token',
						jwtid: JWT_SECRET + Date.now(),
						expiresIn: REFRESH_EXPIRES_IN
					})
				});
				break;

			default:
				res.status(403).end();
				break;
		}
	} else {
		res.status(400).end();
	}
});

//ROUTING - Referesh a request token - POST
server.post('/refreshToken', function(req, res) {
	var request_token, refresh_token;

	//get request_token
	//For the request token, this will be passed through body, instead of header,
	request_token = req.body.request_token;
	//get refresh_token
	refresh_token = req.body.refresh_token;

	//verify refresh_token is still valid
	jwt.verify(refresh_token, JWT_SECRET, function(err, decoded) {
		//if error (expire or invalid)
		if (err) {
			res.status(401).json({
				error: err
			});
		} else {
			try {
				//if the refresh token is still valid, then extend the request token time.
				//decode for request token payload
				var request_token_payload = jwt.decode(request_token);

				request_token_payload.iat = Math.floor(Date.now() / 1000);
				request_token_payload.exp = request_token_payload.iat + REQUEST_EXPIRES_IN;

				res.status(200).json({
					request_token: jwt.sign(request_token_payload, JWT_SECRET)
				});
			} catch (e) {
				console.log(e);
				res.status(401).json({
					error: 'Invalid request token'
				});
			}
		}
	});
});

//ROUTING - Update a post - POST
server.patch('/api/posts/:id', function(req, res, next) {
	//Load database
	var db = fs.readFileSync('db.json');
	db = JSON.parse(db);

	//Current user
	var user = req.user;
	if (!user) {
		user = {
			role: null
		}
	}

	var postId = req.params.id;

	switch (user.role) {
		case 'admin':
			//Admin has full access
			next();
			break;
		case 'user':
			//user can only update and delete his own posts
			//He can see other's posts
			var flag = true;
			_.forEach(db.posts, function(post) {
				if (post.id === parseInt(postId) && post.author === user.username) {
					next();
					flag = false;
					return false; //stop loop
				}
			});
			if (flag) {
				res.status(401).json({
					error: 'You dont have right to update the post of other'
				});
			}
			break;
		default:
			//Guess - they can read only
			//In fact, we can skip this step because if user doesn't login, he can not reach this delete endpoint. 
			res.status(401).json({
				error: 'You dont have right to update this post because of lacking permission'
			});
			break;
	}
});

//ROUTING - Update a post - POST
server.put('/api/posts/:id', function(req, res, next) {
	if (!req.body.hasOwnProperty('title')) {
		res.status(400).json({
			error: 'Missed parameter title'
		});
	} else if (!req.body.hasOwnProperty('content')) {
		res.status(400).json({
			error: 'Missed parameter content'
		});
	} else {
		//Load database
		var db = fs.readFileSync('db.json');
		db = JSON.parse(db);

		//Current user
		var user = req.user;
		if (!user) {
			user = {
				role: null
			}
		}

		var postId = req.params.id;

		switch (user.role) {
			case 'admin':
				//Admin has full access
				next();
				break;
			case 'user':
				//user can only update and delete his own posts
				//He can see other's posts
				var flag = true;
				_.forEach(db.posts, function(post) {
					if (post.id === parseInt(postId) && post.author === user.username) {
						next();
						flag = false;
						return false; //stop loop
					}
				});
				if (flag) {
					res.status(401).json({
						error: 'You dont have right to update the post of other'
					});
				}
				break;
			default:
				//Guess - they can read only
				//In fact, we can skip this step because if user doesn't login, he can not reach this delete endpoint. 
				res.status(401).json({
					error: 'You dont have right to update this post because of lacking permission'
				});
				break;
		}
	}
});

//ROUTING - Delete a post - POST
server.delete('/api/posts/:id', function(req, res, next) {
	//Load database
	var db = fs.readFileSync('db.json');
	var db = JSON.parse(db);
	//Current user
	var user = req.user;
	if (!user) {
		user = {
			role: null
		}
	}

	var postId = req.params.id;

	switch (user.role) {
		case 'admin':
			//Admin has full access
			next();
			break;
		case 'user':
			//user can only update and delete his own posts
			//He can see other's posts
			var flag = true;
			_.forEach(db.posts, function(post) {
				console.log(post.id, postId, post.author, user.username);
				if (post.id === parseInt(postId) && post.author === user.username) {
					next();
					flag = false;
					return false; //stop loop
				}
			});
			if (flag) {
				res.status(401).json({
					error: 'You dont have right to delete the post of other'
				});
			}
			break;
		default:
			//Guess - they can read only
			//In fact, we can skip this step because if user doesn't login, he can not reach this delete endpoint. 
			res.status(401).json({
				error: 'You dont have right to delete this post because of lacking permission'
			});
			break;
	}
});

//ROUTING - Create new post - POST
server.post('/api/posts', function(req, res, next) {
	if (!req.body.hasOwnProperty('title')) {
		res.status(400).json({
			error: 'Missed parameter title'
		});
	} else if (!req.body.hasOwnProperty('content')) {
		res.status(400).json({
			error: 'Missed parameter content'
		});
	} else {
		req.body.author = req.user.username;
		next();
	}
});


//ROUTING - JSON-SERVER
//Monting the routes of jsonServer on /api/
var jsonServerRouter = jsonServer.router('db.json');
server.use('/api/', jsonServerRouter);

jsonServerRouter.render = function(req, res) {
	if ((req.path === '/posts' || req.path === '/posts/') && req.method === 'GET') {
		var user = null;
		if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
			user = jwt.decode(req.headers.authorization.split(' ')[1]);
		}
		if (!user) {
			user = {
				role: null //Set default user role as guess
			};
		}
		var posts = [];
		switch (user.role) {
			case 'admin':
				//Admin has full access
				_.forEach(res.locals.data, function(post) {
					post.dttbNotAllowUpdating = false;
					post.dttbNotAllowDeleting = false;
					posts.push(post);
				});
				break;
			case 'user':
				//user can only update and delete his own posts
				//He can see other's posts
				_.forEach(res.locals.data, function(post) {
					if (post.author === user.username) {
						post.dttbNotAllowUpdating = false;
						post.dttbNotAllowDeleting = false;
					} else {
						post.dttbNotAllowUpdating = true;
						post.dttbNotAllowDeleting = true;
					}
					posts.push(post);
				});
				break;
			default:
				//Guess - they can read only
				_.forEach(res.locals.data, function(post) {
					post.dttbNotAllowUpdating = true;
					post.dttbNotAllowDeleting = true;
					posts.push(post);
				});
				break;
		}
		res.jsonp(posts);
	} else {
		res.jsonp(res.locals.data);
	}
}

//RUN SERVER
server.listen(server.get('port'), function() {
	console.log('dttb-nodejs-api-auth is running at port ' + server.get('port'));
});